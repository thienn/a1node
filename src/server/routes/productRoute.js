'use strict';

var express = require('express');
var Q = require('q');
var db = require('../database/dbConnection');
var productService = require('../services/productService');
var productReviewService = require('../services/productReviewService');

var router = express.Router();

router.get('/', function (req, res, next) {
    var connection = null,
        category = req.query.category;

    db.getConnection()
    .then(function (conn) {
        connection = conn;
        return productService.getProducts(connection, category);
    })
    .then(function(data) {
        res.json(data);
    })
    .catch(function (err) {
        next(err);
    })
    .finally(function(){
        if(connection) connection.release();
    })
    .done();
});

router.get('/:id', function (req, res, next) {
    var connection = null,
        id = req.params.id;

    db.getConnection()
    .then(function (conn) {
        connection = conn;
        return productService.getProduct(connection, id);
    })
    .then(function(data) {
        res.json(data[0]);
    })
    .catch(function (err) {
        next(err);
    })
    .finally(function(){
        if(connection) connection.release();
    })
    .done();
});

router.get('/:id/reviews', function (req, res, next) {
    var connection = null,
        id = req.params.id;
    
    db.getConnection()
    .then(function(conn) {
        connection = conn;
        return productReviewService.getReviews(connection, id);
    })
    .then(function(data) {
        res.json(data);
    })
    .catch(function (err) {
        next(err);
    })
    .finally(function(){
        if(connection) connection.release();
    })
    .done();
});

router.post('/:id/reviews', function (req, res, next) {
    var connection = null,
        data = null,        
        id = req.params.id,
        review = req.body;

    review.productId = id;
    db.getConnection()
    .then(function (conn) {
        connection = conn;
        return connection.beginTransaction();
    })
    .then(function() {
        return productReviewService.addReview(connection, review);
    })
    .then(function(result) {
        data = result;
        return connection.commit();
    })
    .then(function () {
        res.json(data)
    })
    .catch(function (err) {
        next(err);
        return connection.rollback();
    })
    .finally(function(){
        if(connection) connection.release();
    })
    .done();
});

module.exports = router;