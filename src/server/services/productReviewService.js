'use strict';

var ProductReviewService = {};

ProductReviewService.getReviews = function getReviews(connection, productId) {
    var sql = "SELECT id, stars, comment, author FROM product_review WHERE product_id = :productId ";
    return connection.query(sql, { productId : productId });
};

ProductReviewService.addReview = function addReview(connection, review) {
    var sql =  "INSERT INTO product_review (product_id, stars, comment, author) VALUES (:productId, :stars, :comment, :author)";
    return connection.query(sql, review);
};

module.exports = ProductReviewService;