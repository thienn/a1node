(function(){
    "use strict";
    
    angular
        .module('AngularDemo', ['ngRoute'])
        .constant('toastr', toastr)
        .config(['$routeProvider', function($routeProvider){
            $routeProvider
                .when('/home', {
                    templateUrl:  'partials/home.html',
                    controller:   'HomeCtrl',
                    controllerAs: 'vm'
                })
                .when('/category/:categoryId', {
                    templateUrl:  'partials/category.html',
                    controller:   'CategoryCtrl',
                    controllerAs: 'vm'
                })
                .when('/products/:id', {
                    templateUrl:  'partials/product-details.html',
                    controller:   'ProductDetailsCtrl',
                    controllerAs: 'vm'
                })
                .otherwise({
                    redirectTo: '/home'
                });
        }]);
})();