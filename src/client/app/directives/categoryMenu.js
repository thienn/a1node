(function () {
    "use strict";

    angular
        .module('AngularDemo')
        .controller('CategoryMenuCtrl', ['errorService', 'categoryService', function(errorService, categoryService){
            var vm = this;

            categoryService
                .getCategories()
                .then(function (result){
                    vm.categories = result.data;
                })
                .catch(function (error) {
                    errorService.catch(error);
                });
        }])
        .directive('categoryMenu', function () {
            return {
                restrict: 'E',
                scope: {},
                templateUrl: '/partials/category-menu.html',
                controller: 'CategoryMenuCtrl',
                controllerAs: 'vm',
                bindToController: true
            };
        });
})();