(function() {
    "use strict";
    
    angular
        .module('AngularDemo')
        .factory('productService',['$http', function ($http) {
            var service = {};

            service.getProducts = function () {
                return $http.get('api/products');
            };

            service.getProduct = function (id) {
                return $http.get('api/products/' + id);
            };

            service.getProductsByCategory = function (category) {
                return $http.get('api/products?category=' + category);
            };

            service.getReviews = function (productId) {
                return $http.get('api/products/'+ productId + '/reviews');
            };

            service.addReview = function (productId, review) {
                return $http.post('api/products/'+ productId +'/reviews', review)
            };

            return service;
        }]);
})();