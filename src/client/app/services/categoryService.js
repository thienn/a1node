(function () {
    "use strict";
    
    angular
        .module('AngularDemo')
        .factory('categoryService', ['$http', function ($http) {
            var service = {};

            service.getCategories = function () {
                return $http.get('api/categories');
            };

            service.getCategory = function (id) {
                return $http.get('api/categories/' + id);
            };

            return service;
        }]);
})();