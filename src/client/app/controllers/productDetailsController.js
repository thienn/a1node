(function () {
    "use strict";
    
    angular
        .module('AngularDemo')
        .controller('ProductDetailsCtrl', ['$routeParams', 'errorService', 'productService', function ($routeParams, errorService, productService) {
            var vm = this;
            vm.product = {};
            vm.review = {};
            vm.productId = $routeParams.id;

            function init() {
                productService
                    .getProduct(vm.productId)
                    .then(function (result) {
                        vm.product = result.data;
                        return productService.getReviews(vm.productId);
                    })
                    .then(function (result) {
                        vm.product.reviews = result.data;
                    })
                    .catch(function (error) {
                        errorService.catch(error);
                    });
            }

            vm.addReview = function () {
                productService
                    .addReview(vm.productId, vm.review)
                    .then(function (data) {
                        vm.product.reviews.push(vm.review);
                        vm.review = {};
                    })
                    .catch(function (error) {
                        errorService.catch(error);
                    });
            };

            init();
        }]);
})();