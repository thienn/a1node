(function () {
    "use strict";
    
    angular
        .module('AngularDemo')
        .controller('CategoryCtrl', ['$routeParams', 'errorService', 'categoryService', 'productService',
            function ($routeParams, errorService, categoryService, productService) {
            
            var vm = this;
            vm.categoryId = $routeParams.categoryId;
            vm.category = {},
            vm.products = [];

            categoryService
                .getCategory(vm.categoryId)
                .then(function (result) {
                    vm.category = result.data;
                    return productService.getProductsByCategory(vm.category.name);
                })
                .then(function (result) {
                    vm.products = result.data;
                })
                .catch(function (error) {
                    errorService.catch(error);
                });
        }]);
})();