(function () {
    "use strict";
    
    angular
        .module('AngularDemo')
        .controller('HomeCtrl', ['productService', 'errorService', function (productService, errorService) {

            var vm = this;
            vm.products = [];

            productService
                .getProducts()
                .then(function (result) {
                    vm.products = result.data;
                })
                .catch(function (error) {
                    errorService.catch(error);
                });
        }]);
})();